module Data.DList

public export
record DList a where
  constructor MkDL
  unDL : List a -> List a

%inline public export total
fromList : List a -> DList a
fromList = MkDL . (++)

%inline public export total
toList : DList a -> List a
toList xs = (unDL xs) []

public export total
empty : DList a
empty = MkDL id

public export total
singleton : a -> DList a
singleton = MkDL . (::)

public export total
cons : a -> DList a -> DList a
cons x xs = MkDL ((x::) . unDL xs)

public export total
snoc : DList a -> a -> DList a
snoc xs x = MkDL (unDL xs . (x::))

public export total
append : DList a -> DList a -> DList a
append xs ys = MkDL (unDL xs . unDL ys)

public export total
concat : List (DList a) -> DList a
concat = foldr append empty

public export total
foldr : (a -> b -> b) -> b -> DList a -> b
foldr f b = foldr f b . toList

Semigroup (DList a) where
  (<+>) = append

Monoid (DList a) where
  neutral = empty

Functor DList where
  map = Data.DList.foldr (cons . f) empty

Applicative DList where
  pure = singleton
  (<*>) = ?ap

Monad DList where
  m >>= k = foldr (append . k) empty m
