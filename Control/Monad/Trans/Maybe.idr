module Control.Monad.Trans.Maybe
import Control.Monad.Trans

-- TODO document this
public export
record MaybeT (m : Type -> Type) a where
  constructor MkMaybeT
  runMaybeT : m (Maybe a)

%inline public export
Functor m => Functor (MaybeT m) where
  map f = MkMaybeT . map (map f) . runMaybeT

%inline public export
Monad m => Applicative (MaybeT m) where
  pure    = MkMaybeT . pure . pure
  f <*> a = MkMaybeT $ runMaybeT f >>= flip map (runMaybeT a) . (<*>)

%inline public export
Monad m => Monad (MaybeT m) where
  join m = MkMaybeT $ runMaybeT m >>= maybe (pure Nothing) runMaybeT

%inline public export
MonadTrans MaybeT where
  lift = MkMaybeT . map Just

%inline public export
liftM : m (Maybe a) -> MaybeT m a
liftM = MkMaybeT
