module Control.Monad.Trans.Either
import Control.Monad.Trans

record EitherT a (m : Type -> Type) b where
  constructor MkEitherT
  runEitherT : m (Either a b)

Functor m => Functor (EitherT a m) where
  map f = MkEitherT . map (map f) . runEitherT

Monad m => Applicative (EitherT a m) where
  pure    = MkEitherT . pure . pure
  f <*> a = MkEitherT $ runEitherT f >>= flip map (runEitherT a) . (<*>)

Monad m => Monad (EitherT a m) where
  m >>= f = MkEitherT $ runEitherT m >>= either (pure . Left) (runEitherT . f)

MonadTrans (EitherT a) where
  lift = MkEitherT . map pure

liftM : m (Either a b) -> EitherT a m b
liftM = MkEitherT
