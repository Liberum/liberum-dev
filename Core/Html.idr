module Core.Html
import Core.Types

%inline public export total
text : (text : String) -> Html
text = HtmlText

namespace Arity3

  %inline public export total
  a : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  a = HtmlElem "a"

  %inline public export total
  abbr : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  abbr = HtmlElem "abbr"

  %inline public export total
  address : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  address = HtmlElem "address"

  %inline public export total
  article : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  article = HtmlElem "article"

  %inline public export total
  aside : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  aside = HtmlElem "aside"

  %inline public export total
  audio : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  audio = HtmlElem "audio"

  %inline public export total
  b : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  b = HtmlElem "b"

  %inline public export total
  bdo : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  bdo = HtmlElem "bdo"

  %inline public export total
  blockquote : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  blockquote = HtmlElem "blockquote"

  %inline public export total
  body : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  body = HtmlElem "body"

  %inline public export total
  br : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  br = HtmlElem "br"

  %inline public export total
  button : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  button = HtmlElem "button"

  %inline public export total
  canvas : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  canvas = HtmlElem "canvas"

  %inline public export total
  caption : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  caption = HtmlElem "caption"

  %inline public export total
  cite : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  cite = HtmlElem "cite"

  %inline public export total
  code : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  code = HtmlElem "code"

  %inline public export total
  col : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  col = HtmlElem "col"

  %inline public export total
  colgroup : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  colgroup = HtmlElem "colgroup"

  %inline public export total
  datalist : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  datalist = HtmlElem "datalist"

  %inline public export total
  dd : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  dd = HtmlElem "dd"

  %inline public export total
  del : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  del = HtmlElem "del"

  %inline public export total
  details : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  details = HtmlElem "details"

  %inline public export total
  dfn : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  dfn = HtmlElem "dfn"

  %inline public export total
  div : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  div = HtmlElem "div"

  %inline public export total
  dl : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  dl = HtmlElem "dl"

  %inline public export total
  dt : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  dt = HtmlElem "dt"

  %inline public export total
  em : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  em = HtmlElem "em"

  %inline public export total
  embed : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  embed = HtmlElem "embed"

  %inline public export total
  fieldset : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  fieldset = HtmlElem "fieldset"

  %inline public export total
  figcaption : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  figcaption = HtmlElem "figcaption"

  %inline public export total
  figure : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  figure = HtmlElem "figure"

  %inline public export total
  footer : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  footer = HtmlElem "footer"

  %inline public export total
  form : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  form = HtmlElem "form"

  %inline public export total
  h1 : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h1 = HtmlElem "h1"

  %inline public export total
  h2 : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h2 = HtmlElem "h2"

  %inline public export total
  h3 : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h3 = HtmlElem "h3"

  %inline public export total
  h4 : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h4 = HtmlElem "h4"

  %inline public export total
  h5 : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h5 = HtmlElem "h5"

  %inline public export total
  h6 : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h6 = HtmlElem "h6"

  %inline public export total
  header : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  header = HtmlElem "header"

  %inline public export total
  hr : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  hr = HtmlElem "hr"

  %inline public export total
  i : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  i = HtmlElem "i"

  %inline public export total
  iframe : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  iframe = HtmlElem "iframe"

  %inline public export total
  img : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  img = HtmlElem "img"

  %inline public export total
  imput : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  imput = HtmlElem "imput"

  %inline public export total
  ins : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  ins = HtmlElem "ins"

  %inline public export total
  kbd : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  kbd = HtmlElem "kbd"

  %inline public export total
  keygen : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  keygen = HtmlElem "keygen"

  %inline public export total
  label : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  label = HtmlElem "label"

  %inline public export total
  legend : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  legend = HtmlElem "legend"

  %inline public export total
  li : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  li = HtmlElem "li"

  %inline public export total
  main : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  main = HtmlElem "main"

  %inline public export total
  mark : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  mark = HtmlElem "mark"

  %inline public export total
  math : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  math = HtmlElem "math"

  %inline public export total
  menu : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  menu = HtmlElem "menu"

  %inline public export total
  menuitem : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  menuitem = HtmlElem "menuitem"

  %inline public export total
  meter : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  meter = HtmlElem "meter"

  %inline public export total
  nav : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  nav = HtmlElem "nav"

  %inline public export total
  ol : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  ol = HtmlElem "ol"

  %inline public export total
  onject : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  onject = HtmlElem "onject"

  %inline public export total
  optgroup : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  optgroup = HtmlElem "optgroup"

  %inline public export total
  option : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  option = HtmlElem "option"

  %inline public export total
  output : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  output = HtmlElem "output"

  %inline public export total
  p : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  p = HtmlElem "p"

  %inline public export total
  param : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  param = HtmlElem "param"

  %inline public export total
  pre : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  pre = HtmlElem "pre"

  %inline public export total
  progress : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  progress = HtmlElem "progress"

  %inline public export total
  q : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  q = HtmlElem "q"

  %inline public export total
  rp : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  rp = HtmlElem "rp"

  %inline public export total
  rt : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  rt = HtmlElem "rt"

  %inline public export total
  ruby : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  ruby = HtmlElem "ruby"

  %inline public export total
  s : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  s = HtmlElem "s"

  %inline public export total
  samp : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  samp = HtmlElem "samp"

  %inline public export total
  section : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  section = HtmlElem "section"

  %inline public export total
  select : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  select = HtmlElem "select"

  %inline public export total
  small : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  small = HtmlElem "small"

  %inline public export total
  source : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  source = HtmlElem "source"

  %inline public export total
  span : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  span = HtmlElem "span"

  %inline public export total
  strong : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  strong = HtmlElem "strong"

  %inline public export total
  sub : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  sub = HtmlElem "sub"

  %inline public export total
  summary : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  summary = HtmlElem "summary"

  %inline public export total
  sup : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  sup = HtmlElem "sup"

  %inline public export total
  table : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  table = HtmlElem "table"

  %inline public export total
  tbody : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  tbody = HtmlElem "tbody"

  %inline public export total
  td : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  td = HtmlElem "td"

  %inline public export total
  textarea : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  textarea = HtmlElem "textarea"

  %inline public export total
  tfoot : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  tfoot = HtmlElem "tfoot"

  %inline public export total
  th : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  th = HtmlElem "th"

  %inline public export total
  thead : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  thead = HtmlElem "thead"

  %inline public export total
  time : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  time = HtmlElem "time"

  %inline public export total
  tr : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  tr = HtmlElem "tr"

  %inline public export total
  track : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  track = HtmlElem "track"

  %inline public export total
  u : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  u = HtmlElem "u"

  %inline public export total
  ul : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  ul = HtmlElem "ul"

  %inline public export total
  var : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  var = HtmlElem "var"

  %inline public export total
  video : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  video = HtmlElem "video"

  %inline public export total
  wbr : (events : List Event)
     -> (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  wbr = HtmlElem "wbr"

namespace Arity2

  %inline public export total
  a :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  a = a []

  %inline public export total
  abbr :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  abbr = abbr []

  %inline public export total
  address :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  address = address []

  %inline public export total
  article :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  article = article []

  %inline public export total
  aside :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  aside = aside []

  %inline public export total
  audio :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  audio = audio []

  %inline public export total
  b :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  b = b []

  %inline public export total
  bdo :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  bdo = bdo []

  %inline public export total
  blockquote :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  blockquote = blockquote []

  %inline public export total
  body :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  body = body []

  %inline public export total
  br :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  br = br []

  %inline public export total
  button :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  button = button []

  %inline public export total
  canvas :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  canvas = canvas []

  %inline public export total
  caption :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  caption = caption []

  %inline public export total
  cite :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  cite = cite []

  %inline public export total
  code :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  code = code []

  %inline public export total
  col :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  col = col []

  %inline public export total
  colgroup :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  colgroup = colgroup []

  %inline public export total
  datalist :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  datalist = datalist []

  %inline public export total
  dd :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  dd = dd []

  %inline public export total
  del :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  del = del []

  %inline public export total
  details :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  details = details []

  %inline public export total
  dfn :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  dfn = dfn []

  %inline public export total
  div :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  div = div []

  %inline public export total
  dl :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  dl = dl []

  %inline public export total
  dt :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  dt = dt []

  %inline public export total
  em :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  em = em []

  %inline public export total
  embed :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  embed = embed []

  %inline public export total
  fieldset :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  fieldset = fieldset []

  %inline public export total
  figcaption :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  figcaption = figcaption []

  %inline public export total
  figure :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  figure = figure []

  %inline public export total
  footer :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  footer = footer []

  %inline public export total
  form :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  form = form []

  %inline public export total
  h1 :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h1 = h1 []

  %inline public export total
  h2 :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h2 = h2 []

  %inline public export total
  h3 :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h3 = h3 []

  %inline public export total
  h4 :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h4 = h4 []

  %inline public export total
  h5 :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h5 = h5 []

  %inline public export total
  h6 :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  h6 = h6 []

  %inline public export total
  header :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  header = header []

  %inline public export total
  hr :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  hr = hr []

  %inline public export total
  i :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  i = i []

  %inline public export total
  iframe :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  iframe = iframe []

  %inline public export total
  img :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  img = img []

  %inline public export total
  imput :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  imput = imput []

  %inline public export total
  ins :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  ins = ins []

  %inline public export total
  kbd :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  kbd = kbd []

  %inline public export total
  keygen :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  keygen = keygen []

  %inline public export total
  label :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  label = label []

  %inline public export total
  legend :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  legend = legend []

  %inline public export total
  li :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  li = li []

  %inline public export total
  main :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  main = main []

  %inline public export total
  mark :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  mark = mark []

  %inline public export total
  math :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  math = math []

  %inline public export total
  menu :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  menu = menu []

  %inline public export total
  menuitem :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  menuitem = menuitem []

  %inline public export total
  meter :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  meter = meter []

  %inline public export total
  nav :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  nav = nav []

  %inline public export total
  ol :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  ol = ol []

  %inline public export total
  onject :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  onject = onject []

  %inline public export total
  optgroup :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  optgroup = optgroup []

  %inline public export total
  option :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  option = option []

  %inline public export total
  output :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  output = output []

  %inline public export total
  p :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  p = p []

  %inline public export total
  param :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  param = param []

  %inline public export total
  pre :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  pre = pre []

  %inline public export total
  progress :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  progress = progress []

  %inline public export total
  q :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  q = q []

  %inline public export total
  rp :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  rp = rp []

  %inline public export total
  rt :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  rt = rt []

  %inline public export total
  ruby :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  ruby = ruby []

  %inline public export total
  s :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  s = s []

  %inline public export total
  samp :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  samp = samp []

  %inline public export total
  section :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  section = section []

  %inline public export total
  select :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  select = select []

  %inline public export total
  small :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  small = small []

  %inline public export total
  source :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  source = source []

  %inline public export total
  span :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  span = span []

  %inline public export total
  strong :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  strong = strong []

  %inline public export total
  sub :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  sub = sub []

  %inline public export total
  summary :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  summary = summary []

  %inline public export total
  sup :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  sup = sup []

  %inline public export total
  table :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  table = table []

  %inline public export total
  tbody :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  tbody = tbody []

  %inline public export total
  td :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  td = td []

  %inline public export total
  textarea :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  textarea = textarea []

  %inline public export total
  tfoot :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  tfoot = tfoot []

  %inline public export total
  th :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  th = th []

  %inline public export total
  thead :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  thead = thead []

  %inline public export total
  time :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  time = time []

  %inline public export total
  tr :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  tr = tr []

  %inline public export total
  track :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  track = track []

  %inline public export total
  u :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  u = u []

  %inline public export total
  ul :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  ul = ul []

  %inline public export total
  var :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  var = var []

  %inline public export total
  video :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  video = video []

  %inline public export total
  wbr :  (attribs : List Attribute)
     -> (html : List Html)
     -> Html
  wbr = wbr []

