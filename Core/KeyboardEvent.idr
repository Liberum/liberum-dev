module Core.KeyboardEvent
import Core.Utils
import Core.Javascript
import Core.Javascript.Native
import Control.Monad.Trans.Maybe
import Control.Monad.Trans

%default covering
%language ElabReflection

public export
record KeyboardEvent where
  constructor MkKeyboardEvent
  altKey      : Bool
  altGrKey    : Bool
  shiftKey    : Bool
  metaKey     : Bool
  repeat      : Bool
  key         : String
  code        : String
  isComposing : Bool
  location    : Integer

decEqKeyboardEvent : (x, y : KeyboardEvent) -> Dec (x = y)
%runElab (deriveDecEq `{decEqKeyboardEvent})

||| Parse a keyboard event from a Javascript reference
export
parseKeyboardEvent : JSRef -> JS_IO (Maybe KeyboardEvent)
parseKeyboardEvent p =
  do JSObject "KeyboardEvent" <- typeof p | _ => pure Nothing
     runMaybeT $ do
       altKey   <- prop Bool   "altKey"      p
       altGrKey <- prop Bool   "altGraphKey" p
       shiftKey <- prop Bool   "shiftKey"    p
       metaKey  <- prop Bool   "metaKey"     p
       repeat   <- prop Bool   "repeat"      p
       key      <- prop String "key"         p
       code     <- prop String "code"        p
       isCom    <- prop Bool   "isComposing" p
       location <- prop Double "location"    p

       pure $ MkKeyboardEvent
         altKey  altGrKey shiftKey
         metaKey repeat   key
         code    isCom    (cast location)

