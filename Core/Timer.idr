module Core.Timer
import Core.Utils
import Core.Javascript

export
record Interval where
  constructor MkInterval
  unInterval : JSRef

export
record Timeout where
  constructor MkTimeout
  unTimeout : JSRef

export
setInterval : (() -> JS_IO ()) -> Int -> JS_IO Interval
setInterval fn time =
  MkInterval <$> jscall "setInterval(%0,%1)"
                        (JsFn (() -> JS_IO ()) -> Int -> JS_IO JSRef)
                        (MkJsFn fn) time

export
clearInterval : Interval -> JS_IO ()
clearInterval = jscall "clearInterval(%0)" (JSRef  -> JS_IO ()) . unInterval

export
setTimeout : (() -> JS_IO ()) -> Int -> JS_IO Timeout
setTimeout fn time =
  MkTimeout <$> jscall "setTimeout(%0,%1)"
                      (JsFn (() -> JS_IO ()) -> Int -> JS_IO JSRef)
                      (MkJsFn fn) time

export
clearTimeout : Timeout -> JS_IO ()
clearTimeout = jscall "clearTimeout(%0)" (JSRef  -> JS_IO ()) . unTimeout


