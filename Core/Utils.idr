||| Useful functions which don't really fin anywere else yet are extremely useful in
||| practive. This also includes common modules exported for convenience.
module Utils

import public Pruviloj.Derive.DecEq
import public Control.Monad.Syntax
import public Control.Pipeline
import public Control.Delayed
import public Control.Category
import public Data.Complex
import public Data.Morphisms
import public Data.String
import public Language.JSON
import public Language.Reflection
import public Language.Reflection.Utils
import Text.PrettyPrint.WL -- Unit tests
import Data.IORef

%default covering -- majority here are partial
%language ElabReflection

||| Call a native javascript procedure
%inline public export
jscall : (name : String) -> (type : Type) -> { auto p : FTy FFI_JS [] type } -> type
jscall name type = foreign FFI_JS name type

||| Index a list given a constructor/function and a stream of values
public export total
indexdWith : (f : (a -> b -> c)) -> Stream a -> List b -> List c
indexdWith _ _ [] = []
indexdWith f (x :: xs) (y :: ys) = f x y :: indexdWith f xs ys

||| Index a list elements with their position
public export total
indexed : List a -> List (Integer, a)
indexed = indexdWith MkPair [0..]

public export
consoleLog : String -> JS_IO ()
consoleLog = jscall "console.log(%0)" (Ptr -> JS_IO ()) . believe_me

||| Print a message for debugging with source location as a side effect
|||
||| @loc call site location
||| @message message to display
||| @result the result returned
public export total
trace : {default (%runElab sourceLocation) loc : SourceLocation}
     -> (message : String)
     -> (result : a)
     -> a
trace {loc} x v = unsafePerformIO {ffi=FFI_JS}
                  (consoleLog ("[TRACE: " ++ show loc ++ "] " ++ x) *> pure v)
-- Flip this on when staging for production since we do not want to leave trace
-- in client code
-- %deprecated trace "Debug function `trace' used in source file"


