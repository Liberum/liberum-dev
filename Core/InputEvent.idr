module Core.InputEvent
import Core.Utils
import Core.Javascript
import Core.Javascript.Native
import Control.Monad.Trans.Maybe
import Control.Monad.Trans

%language ElabReflection

public export
record InputEvent where
  constructor MkInputEvent
  inputType    : String
  data'        : String
  dataTransfer : Unit -- TODO: DataTransfer
  isComposing  : Bool

decEqInputEvent : (x, y : InputEvent) -> Dec (x = y)
%runElab (deriveDecEq `{decEqInputEvent})

public export total
DecEq InputEvent where
  decEq = decEqInputEvent

export
parseInputEvent : JSRef -> JS_IO (Maybe InputEvent)
parseInputEvent p =
  do JSObject "InputEvent" <- typeof p
     runMaybeT $ do
       type  <- prop String "inputType"   p
       data' <- prop String "data"        p
       isCom <- prop Bool   "isComposing" p
       -- TODO DataTransfer
       pure $ MkInputEvent type data' () isCom

%fragile parseInputEvent "TODO: DataTransfer"
