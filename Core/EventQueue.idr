module Core.EventQueue
import Core.Utils
import Data.IORef
-- Functions for managing the internal event queue

||| Internal event queue filled with user specified events for use in the main
||| event-loop
export
record EventQueue a where
  constructor MkEP
  -- This works as Javascript runs on a single thread thus no locks are required as
  -- long as it's write-only. This includes all callback actions such as an async
  -- request for an external resource.
  queue : IORef (List a)

||| Return a fresh event queue
export total
init : JS_IO (EventQueue a)
init = MkEP <$> newIORef' []

||| Push a new event on to the event queue
export total -- TODO refactor
push : EventQueue a -> a -> JS_IO ()
push (MkEP queue) value = modifyIORef' queue (value::)

||| Pop an event off from the event queue
export total -- TODO refactor
pop : EventQueue a -> JS_IO (Maybe a)
pop (MkEP queue) =
  do val <- head' <$> readIORef' queue
     modifyIORef' queue (maybe [] id . tail')
     pure val

-- TODO onevent
