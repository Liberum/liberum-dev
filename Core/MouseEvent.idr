module Core.MouseEvent
import Core.Utils
import Core.Javascript
import Core.Javascript.Native
import Control.Monad.Trans.Maybe
import Control.Monad.Trans

%language ElabReflection

public export
data MouseButton = Primary | Auxiliary | Secondary

public export
fromMouseButton : MouseButton -> Int
fromMouseButton Primary   = 0
fromMouseButton Auxiliary = 1
fromMouseButton Secondary = 2

decEqMouseButton : (x, y : MouseButton) -> Dec (x = y)
%runElab (deriveDecEq `{decEqMouseButton})

public export total
DecEq MouseButton where
  decEq = decEqMouseButton

public export
Eq MouseButton where
  x == y = fromMouseButton x == fromMouseButton y

public export
record MouseEvent where
  constructor MkMouseEvent
  screenX   : Integer
  screenY   : Integer
  clientX   : Integer
  clientY   : Integer
  movementX : Integer
  movementY : Integer
  region    : Maybe String
  ctrlKey   : Bool
  shiftKey  : Bool
  altKey    : Bool
  metaKey   : Bool
  button    : MouseButton

decEqMouseEvent : (x, y : MouseEvent) -> Dec (x = y)
%runElab (deriveDecEq `{decEqMouseEvent})

public export total
DecEq MouseEvent where
  decEq = decEqMouseEvent

export
parseMouseEvent : JSRef -> JS_IO (Maybe MouseEvent)
parseMouseEvent p =
  do JSObject "MouseEvent" <- typeof p | _ => pure Nothing
     region <- property String "region" p
     runMaybeT $ do
       screenX  <- prop Double "screenX"    p
       screenY  <- prop Double "screenY"    p
       clientX  <- prop Double "clientX"    p
       clientY  <- prop Double "clientY"    p
       moveX    <- prop Double "movementX"  p
       moveY    <- prop Double "movementY"  p
       ctrlKey  <- prop Bool   "ctrlKey"    p
       shiftKey <- prop Bool   "shiftKey"   p
       altKey   <- prop Bool   "altKey"     p
       metaKey  <- prop Bool   "metaKey"    p
       button   <- liftM (aux <$> property Int "button" p)

       pure $ MkMouseEvent
           (cast screenX) (cast screenY)
           (cast clientX) (cast clientY)
           (cast moveX)   (cast moveY)
           region ctrlKey shiftKey
           altKey metaKey button

  where

    aux : Maybe Int -> Maybe MouseButton
    aux (Just 0) = Just Primary
    aux (Just 1) = Just Auxiliary
    aux (Just 2) = Just Secondary
    aux _        = Nothing
