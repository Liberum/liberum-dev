module Core.Javascript
import Core.Utils

public export
JSRef : Type
JSRef = Ptr

||| Convert a javascript object to a string
%inline export
toString : JSRef -> JS_IO String
toString = jscall "%0.toString()" (JSRef  -> JS_IO String)
