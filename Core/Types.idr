module Core.Types
import Core.Utils
import Core.Javascript

public export
record EventOptions where
  constructor MkEventOptions
  passive : Maybe Bool
  ||| Remove the event listener after first invocation
  once    : Maybe Bool
  capture : Maybe Bool

public export total
defaultEventOptions : EventOptions
defaultEventOptions = MkEventOptions Nothing Nothing Nothing

public export
record Event where
  constructor MkEvent
  ||| Name of the event
  event   : String
  ||| Event options
  options : EventOptions
  ||| Event handler taking a pointer to the event object passed when the event is
  ||| triggered
  handler : JSRef -> JS_IO ()

||| Wrapper for raw js object pointers
public export
record Node where
  constructor MkNode
  node : JSRef

public export
Attribute : Type
Attribute = Pair String String

public export
data Html : Type where
  ||| HTML element
  |||
  ||| @name tag name
  ||| @events list of JS_IO events taking a pointer to the event object
  ||| @attribs list node attibutes
  ||| @html list of child nodes
  HtmlElem : (name:String) -> (events:List Event)
                           -> (attribs:List Attribute)
                           -> (html:List Html)
                           -> Html
  ||| HTML text element
  |||
  ||| @text text content
  HtmlText : (text:String) -> Html


