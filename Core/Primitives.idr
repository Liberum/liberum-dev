module Core.Primitives
import Core.Utils
import Core.Types
import Core.Javascript

%inline private
documentBody : JS_IO Node
documentBody = MkNode <$> jscall "document.body" _

%inline private
createTextNode : (text : String) -> JS_IO Node
createTextNode = map MkNode . jscall "document.createTextNode(%0)" _

%inline private
createElement : (tag : String) -> JS_IO Node
createElement = map MkNode . jscall "document.createElement(%0)" _

%inline export partial -- may fail to get the element
getElementById : (id : String) -> JS_IO Node
getElementById = map MkNode . jscall "document.getElementById(%0)" _

%inline export partial
nthChild : (node : Node) -> Int -> JS_IO Node
nthChild (MkNode node) = map MkNode
                       . jscall "%0.childNodes[%1]"
                         (JSRef  -> Int -> JS_IO JSRef)
                         node

%inline export
appendChild : (parent, child : Node) -> JS_IO Node
appendChild (MkNode parent) = map MkNode
                            . jscall "%0.appendChild(%1)"
                                     (JSRef  -> JSRef -> JS_IO JSRef)
                                     parent . node

%inline export
replaceChild : (parent, old, new : Node) -> JS_IO ()
replaceChild (MkNode parent) (MkNode old) =
  jscall "%0.replaceChild(%1,%2)"
         (JSRef  -> JSRef -> JSRef -> JS_IO ())
         parent old . node

%inline export
removeChild : (parent : Node) -> (child : Node) -> JS_IO Node
removeChild (MkNode parent) = map MkNode
                            . jscall "%0.removeChild(%1)"
                                     (JSRef  -> JSRef -> JS_IO JSRef)
                                     parent . node

%inline export
setAttribute : (node : Node) -> (attribute : String) -> (value : String) -> JS_IO ()
setAttribute = jscall "%0.setAttribute(%1, %2)"
                      (JSRef  -> String -> String -> JS_IO ())
             . node

%inline export
removeAttribute : (node : Node) -> (attribute : String) -> JS_IO ()
removeAttribute = jscall "%0.removeAttribute(%1)"
                         (JSRef  -> String -> JS_IO ())
                . node

%inline export partial
jsonParse : String -> JS_IO JSRef
jsonParse = jscall "JSON.parse(%0)" _

export partial
optionsToJSRef  : EventOptions -> JS_IO JSRef
optionsToJSRef  options =
  [ ("once", once options)
  , ("capture", capture options)
  , ("passive", passive options)
  ] |> filter (isJust . snd)
    |> map (\(x,y) => (x, fromMaybe JNull (map JBoolean y)))
    |> JObject
    |> show
    |> jsonParse

export partial
addEventListener : (target  : Node)
               -> (name    : String)
               -> (handler : (JSRef  -> JS_IO ()))
               -> (options : EventOptions)
               -> JS_IO ()
addEventListener (MkNode target) name handler options =
  optionsToJSRef  options >>=
    jscall "%0.addEventListener(%1,%2,%3)"
           (JSRef  -> String -> JsFn (JSRef  -> JS_IO ()) -> JSRef -> JS_IO ())
           target name (MkJsFn handler)

%inline export
removeEventListener : (target  : Node)
                   -> (name    : String)
                   -> (handler : (JSRef  -> JS_IO ()))
                   -> (options : EventOptions)
                   -> JS_IO ()
removeEventListener (MkNode target) name handler options =
  optionsToJSRef  options >>=
    jscall "%0.removeEventListener(%1,%2,%3)"
           (JSRef  -> String -> JsFn (JSRef  -> JS_IO ()) -> JSRef -> JS_IO ())
           target name (MkJsFn handler)

export
addEventHandler : (target : Node) -> (event : Event) -> JS_IO ()
addEventHandler target (MkEvent event options handler) =
  addEventListener target event handler options

export
dispatchEvent : (name : String) -> (bubbles : Bool) -> (target : Node) -> JS_IO Bool
dispatchEvent name bubbles (MkNode target) =
  map (=="true") (jscall "%0.dispatchEvent(new Event(%1,{bubbles:%2==1})).toString()"
                         (JSRef  -> String -> Int -> JS_IO String)
                         target name (if bubbles then 1 else 0))

export
updateAttribs : (target : Node)
             -> (old    : List (String, String))
             -> (new    : List (String, String))
             -> JS_IO ()
updateAttribs target old new =
  do traverse_ (removeAttribute target . fst) old
     traverse_ (uncurry $ setAttribute target) new

export
createDomNode : Html -> JS_IO Node
createDomNode (HtmlElem name events attribs html) =
  do nodes <- traverse createDomNode html
     tag   <- createElement name
     traverse_ (addEventHandler tag) events
     updateAttribs tag [] attribs
     traverse_ (appendChild tag) nodes
     pure tag
createDomNode (HtmlText text) =
  createTextNode text

export
render : (root : Node)
      -> (old : Maybe Html)
      -> (new : Maybe Html)
      -> JS_IO ()
render root old new = renderList (indexed $ toList old) (toList new)

  where

    remove : Integer -> JS_IO ()
    remove n =
      do removeChild root !(nthChild root (fromInteger n))
         pure ()

    renderList : (old : List (Integer, Html)) -> (new : List Html) -> JS_IO ()
    renderList old new = traverse_ (remove . fst) old
                      *> traverse createDomNode new
                      >>= traverse_ (appendChild root)


