module Core.Javascript.Native
import Core.Utils
import Core.Javascript
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
-- Utilities for working with opaque javascript types such as those from native
-- Javascript libraries. There's very little type safety here and most of the
-- functions invlolve IO or raw inline Javascript.

||| Raw Javascript types
public export
data JSType : Type where
  JSNumber    : JSType
  JSString    : JSType
  JSBoolean   : JSType
  JSFunction  : JSType
  JSUndefined : JSType
  ||| An opaque Javascript object
  JSObject    : String -> JSType
  JSNull      : JSType

private
fromJSType : JSType -> Int
fromJSType JSNumber     = 0
fromJSType JSString     = 1
fromJSType JSBoolean    = 2
fromJSType JSFunction   = 3
fromJSType JSUndefined  = 4
fromJSType (JSObject x) = 5
fromJSType JSNull       = 6

public export
Eq JSType where
  x == y = fromJSType x == fromJSType y

||| Retrieve the constructor name of a Javascript object
%inline export
constructorName : JSRef -> JS_IO String
constructorName = jscall "%0.constructor.name" (JSRef  -> JS_IO String)

||| Retrive the type of a raw Javascript object
export
typeof : JSRef -> JS_IO JSType
typeof p = case !(jscall check (JSRef  -> JS_IO Int) p) of
                0 => pure JSNumber
                1 => pure JSString
                2 => pure JSBoolean
                3 => pure JSFunction
                4 => pure JSUndefined
                5 => pure $ JSObject !(constructorName p)
                _ => pure JSNull

  where

    check : String
    check ="""
(function (p) {
  if (typeof p == 'number') return 0;
  if (typeof p == 'string') return 1;
  if (typeof p == 'boolean') return 2;
  if (typeof p == 'function') return 3;
  if (typeof p == 'undefined') return 4;
  if (typeof p == 'object') return 5;
  return 6;})(%0)"""

||| Convert to and from Javascript and Idris types
public export
interface ToJSRef a where
  toJSRef : a -> JSRef

||| Defualt ToJSRef implementation
%inline
defaultToJSRef : ToJSRef from => from -> JSRef
defaultToJSRef = believe_me

export
ToJSRef String where
  toJSRef = defaultToJSRef

export
ToJSRef Double where
  toJSRef = defaultToJSRef

export
ToJSRef Int where
  toJSRef = defaultToJSRef

export
ToJSRef Char where
  toJSRef = defaultToJSRef

export
ToJSRef Bool where
  toJSRef = defaultToJSRef

public export
interface FromJSRef to where
  fromJSRef : JSRef -> to

||| Default FromJSRef implementation
%inline
total
defaultFromJSRef : JSRef -> to
defaultFromJSRef = believe_me

export
FromJSRef String where
  fromJSRef = defaultFromJSRef

export
FromJSRef Double where
  fromJSRef = defaultFromJSRef

export
FromJSRef Int where
  fromJSRef = defaultFromJSRef

export
FromJSRef Char where
  fromJSRef = defaultFromJSRef

export
FromJSRef Bool where
  fromJSRef = defaultFromJSRef

public export
interface FromJSRef to => SafeFromJSRef to where
  safeFromJSRef : JSRef -> JS_IO (Maybe to)

defaultSafeFromJSRef : SafeFromJSRef to => JSType -> JSRef -> JS_IO (Maybe to)
defaultSafeFromJSRef jstype p =
  if !(typeof p) == jstype
     then pure . Just $ fromJSRef p
     else pure Nothing

export
SafeFromJSRef String where
  safeFromJSRef = defaultSafeFromJSRef JSString

export
SafeFromJSRef Double where
  safeFromJSRef = defaultSafeFromJSRef JSNumber

export
SafeFromJSRef Int where
  safeFromJSRef = defaultSafeFromJSRef JSNumber

export
SafeFromJSRef Char where
  safeFromJSRef = defaultSafeFromJSRef JSString

export
SafeFromJSRef Bool where
  safeFromJSRef = defaultSafeFromJSRef JSBoolean

||| Access a Javascript object's properties while ensuring
||| they are of the correct type
export
property : (to : Type)
        -- since there's no syntax for fetching the dictionary based on input
        -- type we must do it ourselves using a default argument
        -> {default %implementation x : SafeFromJSRef to}
        -> String
        -> JSRef -> JS_IO (Maybe to)
property _ prop p = jscall ("(%0)." ++ prop) (JSRef -> JS_IO JSRef) p
                >>= safeFromJSRef

export
prop : (to : Type)
     -> {default %implementation x : SafeFromJSRef to}
     -> String
     -> JSRef -> MaybeT JS_IO to
prop t prp p = liftM $ property t prp p

