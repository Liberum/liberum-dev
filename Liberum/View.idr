module View
import Core.Types
import Core.Html
import Core.EventQueue
import Core.Javascript

import Liberum.Model
import Liberum.Pages

parameters (evq : EventQueue Application)
  public export
  initial : Html
  initial =
    frame
      evq
      []
      [div
        [("class", "init-box")]
        [text
          ("Liberum is now initialized, " ++
            "press the button below to continue the demo.")
        ,br [] []
        ,br [] []
        ,button
          [MkEvent
            "click"
            defaultEventOptions
            (const $ push evq $ User $ UserRequestPage ThreadViewer)]
          []
          [text "Continue..."]]]
