module Main
import Core.Utils
import Core.Primitives
import Core.EventQueue
import Core.Types

import Liberum.View
import Liberum.View.ThreadViewer
import Liberum.Model
import Liberum.Types
import Liberum.Controller

main : JS_IO ()
main =
  do evq <- init {a=Application}
     root <- getElementById "app-root"
     render root Nothing (Just $ initial evq)
     app root (initial evq) evq
