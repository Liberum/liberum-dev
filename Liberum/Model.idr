module Model

public export
record UserQuery where
  constructor MkUQ

public export
data Page
  = Initialize
  | About
  | Favourites
  | ThreadSearch
  | ThreadViewer
  | MessageEditor
  | Contact
  | Settings

public export
data RibbonAction
  = RibbonIconClicked

public export
data UserAction
  = UserRequestPage Page
  | UserRibbonAction RibbonAction

public export
data Application
  = None
  | Query   UserQuery
  | User    UserAction
  | Request
