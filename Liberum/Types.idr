module Liberum.Types
import Core.Types
import Core.Html
import Core.EventQueue
import Core.Javascript
import Data.String.Extra
import Data.SortedSet

public export
record UserData where
  constructor MkUserData
  name : String
  image : String
  pubkey : String

public export
record UserPrefs where
  constructor MkUserPrefs
  userdata : UserData
  favourites : SortedSet String
  blacklist : SortedSet String
  suppressed : SortedSet String
  location : String
  maxThreads : Int
  maxMsgs : Int

public export
record Tags where
  constructor MkTags
  key : String
  taglist : List String
  checksum : String

public export
record Message where
  constructor MkMessage
  userref : Nat
  encryptedMessage : String
  msgchecksums : List Attribute
  msgtags : List Tags

public export
record Thread where
  constructor MkThread
  title : String
  serverkey : String
  users : List UserData
  messages : List Message
  threadtags : List Tags

public export
data FilteredMessage = OK Message
                     | Blacklisted (List Tags)

public export
data Dimension : Type where
  Pixels : Int -> Dimension
  Percent : Int -> Dimension

public export
Show Dimension where
  show (Pixels d) = show d ++ "px"
  show (Percent d) = show d ++ "%"

public export
record Image where
  constructor MkImage
  src : String
  width : Dimension
  height : Dimension

-- Image --

public export
staticImage : Image -> Html
staticImage image =
  img
    [("src", src image)
    ,("width", show $ width image)
    ,("height", show $ height image)]
    []

public export
activeIcon : List Event
    -> (image : Image)
    -> (hover : String)
    -> (cssClass : String)
    -> Html
activeIcon evnt image hover css =
  button
    evnt
    [("title", hover), ("class", css)]
    [staticImage image]

public export
linkImage : String -> String -> Image -> Html
linkImage name url image =
  a [("title",name),("href",url),("target","_blank")] [staticImage image]

-- UserData --

public export
default_user : UserData
default_user = (MkUserData "Anon" "image/Anon.png" "")

public export
usertile : UserData -> Html
usertile ud =
  div
    [("class", "usertile")]
    [div
      [("class", "usertile-icon")]
      [staticImage
        (MkImage (image ud) (Pixels 80) (Pixels 80))]
    ,table
      [("class", "usertile-table")]
      [tr
        [("class", "usertile-name")]
        [text $ name ud]
      ,tr
        [("class", "usertile-key")]
        [text $ (take 20 (pubkey ud)) ++ ".."]]]

-- Tag --

export
encloseTag : List String -> UserPrefs -> List Html
encloseTag tags userprefs = map toHtml tags

  where

    toHtml : String -> Html
    toHtml tag =
      div
        [MkPair
          "class"
          (if contains tag (blacklist userprefs)
             then "tag tag-blacklist"
             else
               if contains tag (favourites userprefs)
                 then "tag tag-favourite"
                 else "tag")]
        [text tag]

export
encloseThreadTags : Tags -> UserPrefs -> String -> Html 
encloseThreadTags tag userprefs serverkey = 
  div
    [MkPair
      "class"
      (if (key tag) == serverkey 
         then "thread-tag-box thread-tag-serverbox" 
         else "thread-tag-box thread-tag-userbox")] 
    [div [("class","thread-tag-key")] [text $ take 30 (key tag)] 
    ,div [("class","thread-tag-flex")] (encloseTag (taglist tag) userprefs)] 

export
encloseMsgTags : Tags -> UserPrefs -> String -> Html 
encloseMsgTags tag userprefs serverkey = 
  div
    [MkPair
      "class"
      (if (key tag) == serverkey 
        then "message-tag-box message-tag-serverbox"
        else "message-tag-box message-tag-userbox")]
    [div [("class","message-tag-flex")] $ encloseTag (taglist tag) userprefs]

public export
threadTags : Thread -> UserPrefs -> List Html
threadTags thread userprefs =
  map toHtml (threadtags thread)
    
    where
      
      toHtml : Tags -> Html
      toHtml tags = encloseThreadTags tags userprefs (serverkey thread)

-- Message --

export
decryptedMessage : Message -> Html
decryptedMessage msg =
  div
    [("class","message-text")]
    (intersperse
      (br [] [])
      $ map text $ lines $ encryptedMessage msg)

tile : Message -> Thread -> Html
tile msg thread =
  div [("class","message-usertile")]
      (maybe ([usertile $ default_user])
             (pure . usertile) $ index' (userref msg) (users thread))

serverStyle : Message -> Thread -> String
serverStyle msg thread =
  if Just (serverkey thread) == map fst (last' (msgchecksums msg))
     then "message server-signed-message"
     else "message"

export
msgFilter : List Message -> UserPrefs -> List FilteredMessage
msgFilter [] _ = []
msgFilter msgs prefs =
  map applyBlacklist msgs -- $ filter (\m => hasSuppressedTags $ msgtags m) msgs

  where

    blacklistTagFilter : Tags -> Tags
    blacklistTagFilter tags =
      (MkTags
        (key tags)
        (filter (\t => contains t $ blacklist prefs) $ taglist tags)
        (checksum tags))

    suppressedTagFilter : Tags -> Bool
    suppressedTagFilter tags =
      isCons $ filter (\t => contains t $ suppressed prefs) $ taglist tags

    hasSuppressedTags : List Tags -> Bool
    hasSuppressedTags tags =
      elem True $ map suppressedTagFilter tags

    applyFilter : Message -> List Tags -> FilteredMessage
    applyFilter msg tagset =
      if (elem True $ map (\t => isCons $ taglist t) $ tagset)
         then (Blacklisted tagset)
         else (OK msg)
    
    applyBlacklist : Message -> FilteredMessage
    applyBlacklist msg =
      applyFilter msg $ map (blacklistTagFilter) $ msgtags msg

tagToHtml : Tags -> Thread -> UserPrefs -> Html
tagToHtml tag thread prefs = encloseMsgTags tag prefs (serverkey thread)

export
encapsulate : Thread -> UserPrefs -> FilteredMessage -> Html
encapsulate thread prefs (OK msg) =
  div
    [MkPair "class" (serverStyle msg thread)]
    [div
      [("class","message-userdata-tag-container")]
      [tile msg thread
      ,div
        [("class","message-tag-container")]
        (map (\t => tagToHtml t thread prefs) $ msgtags msg)]
    ,decryptedMessage msg]
encapsulate thread prefs (Blacklisted tags) =
  div
    [("class","blacklisted")]
    [text
      ("This message has been hidden by your blacklist"
      ++ " due to the following tags: ")
    ,div
      [("class","message-tag-container")]
      (map (\t => tagToHtml t thread prefs) tags)]

-- Demo --

public export
test_users : List UserData
test_users =
    [(MkUserData
      "Aeron The Bunny"
      "image/Aeron-Portrait.jpg"
      "y07809ykqxjjjxj78qj8y79atwa4b4aaj4")
    ,(MkUserData
      "The Rude Dude"
      "image/Anon.png"
      "87n1c787018748n121c134254s7n45n5b2")]

public export
test_thread : Thread
test_thread =
  (MkThread
    "A New Day for LBRY"
    "8978y2jchhdh278x2o78cy072y987jx2f8"
    test_users
    [(MkMessage
      0
      "Hello and Welcome Ladies, Gentlemen and Attack Helecopters, to Liberum. Come here to see all the things you find interesting and share anything that isn't here.\n\nSee the User given tags for this message on the bottom of this post, with Server/Moderator given tags on the bottom right in the blue box."
      [ ("y07809ykqxjjjxj78qj8y79atwa4b4aaj4","MSG CHECKSUM") ]
      [ (MkTags "y07809ykqxjjjxj78qj8y79atwa4b4aaj4" ["Favourite"] "TAG CHECKSUM")
      , (MkTags "8978y2jchhdh278x2o78cy072y987jx2f8" ["Regular"] "TAG CHECKSUM") ] )
    ,(MkMessage
      1
      "Hey, Check out this awesome movie I uploaded without the creator's permission!\n\n[Link removed due to verified DMCA request from copyright holder]"
      [ ("87n1c787018748n121c134254s7n45n5b2","")
      , ("8978y2jchhdh278x2o78cy072y987jx2f8","") ]
      [ (MkTags "87n1c787018748n121c134254s7n45n5b2" [] "TAG CHECKSUM")
      , (MkTags "8978y2jchhdh278x2o78cy072y987jx2f8" [ "DMCA" ] "TAG CHECKSUM" ) ] ) ]
    [ (MkTags "y07809ykqxjjjxj78qj8y79atwa4b4aaj4" ["Favourite"] "TAG CHECKSUM")
    , (MkTags "8978y2jchhdh278x2o78cy072y987jx2f8" ["Regular", "Regular 2", "Regular 3"] "TAG CHECKSUM")])

public export
test_prefs : UserPrefs
test_prefs =
  (MkUserPrefs
     default_user
     (fromList ["Favourite"])
     (fromList ["NSFW", "DMCA"])
     (fromList ["UnderageExplicitMaterial"])
     "UK"
     10
     7)
