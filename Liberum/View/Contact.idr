module Liberum.View.Contact
import Core.EventQueue
import Core.Types
import Core.Html

import Liberum.Model
import Liberum.View.Common

parameters (evq : EventQueue Application)
  contactSideBar : List Html
  contactSideBar =
    [text "General Enquiries"
    ,br [] []
    ,br [] []
    ,text "Director"
    ,br [] []
    ,text "Joshua Lewis Hayes"
    ,br [] []
    ,br [] []
    ,text "Email"
    ,br [] []
    ,a
      [("href","mailto:professordey@liberum.world")]
      [text "professordey@liberum.world"]
    ,br [] []
    ,br [] []
    ,text "Discord ID"
    ,br [] []
    ,text "@ProfessorDey#1578"]

  contactMainSection : List Html
  contactMainSection =
    []

  export
  contactPage : Html
  contactPage =
    frame evq contactSideBar (navBar evq :: contactMainSection)
