module Liberum.View.About
import Core.EventQueue
import Core.Types
import Core.Html

import Liberum.Model
import Liberum.View.Common

parameters (evq : EventQueue Application)
  aboutSideBar : List Html
  aboutSideBar =
    [div
      []
      [("class","about-sidebar-text")]
      [text
        ("The Liberum Open Forum Project is an open, collaborative effort to "
        ++ "bring a truely free and open forum to the world, built on top of "
        ++ "the LBRY Protocol for it's decentralization and user privacy "
        ++ "protection.")
      ,br [] []
      ,br [] []
      ,text "See our Discord or our Gitlab for more information"]]

  aboutMainSection : List Html
  aboutMainSection =
    [aboutProject]
{-    ,howItWorks
      ,aboutTeam] -}

    where

      section : String -> List Html -> Html
      section title content =
        div
          [("class","about-section")]
          ([h1 [] [text title], br [] []] ++ content)

      aboutProject : Html
      aboutProject =
        section
          "About The Project"
          [text
            ("We started developing Liberum to fill what we found as a dual "
            ++ "need. First was the need to explore the true potential of the "
            ++ "LBRY Protocol, beyond the simple content sharing that it was "
            ++ "marketed as being for. As a sophisticated peer-to-peer file "
            ++ "sharing platform with so much flexibility, why limit ourselves "
            ++ "to merely replicating video/music sharing sites. Every file is "
            ++ "encrypted, verifyable and requires no access to any central "
            ++ "source. One only requires access to the protocol itself and "
            ++ "all of Liberum's content would be accessible, you'd just need "
            ++ "something to display it.")
          ,br [] []
          ,br [] []
          ,text
            ("Second was a need for a modern forum that protects its users "
            ++ "from the endless advance of modern data collection through "
            ++ "decentralised, local only browsing, peer-to-peer content "
            ++ "distribution and self signed messages in every self contained "
            ++ "thread to ensure the authenticity of the data you've received "
            ++ "and of the identity of those you're communicating with while "
            ++ "maintaining as much anonymity as the user prefers.")]

      howItWorks : Html
      howItWorks =
        section
          "How Liberum Works"
          []

      aboutTeam : Html
      aboutTeam =
        section
          "About the Team"
          []

  export
  aboutPage : Html
  aboutPage =
    frame evq aboutSideBar (navBar evq :: aboutMainSection)
