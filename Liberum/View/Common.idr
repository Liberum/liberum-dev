module Liberum.View.Common
import Core.EventQueue
import Core.Utils
import Core.Types
import Core.Html
import Core.Javascript
import Liberum.Types
import Liberum.Model
import Liberum.Utils

%default covering

-- Common Structures --
parameters (evq : EventQueue Application)
  export
  navBar : Html
  navBar =
    div [("class", "navBar")]
        $ map navButton
              [ about
              , favourites
              , viewer
              , search
              , contact
              , initial ]

    where

      PageType : Type
      PageType = (Page, String, String)

      navButton : PageType -> Html
      navButton (page, name, hover) =
        button
          [MkEvent
            "click"
            defaultEventOptions
            (const $ push evq $ User $ UserRequestPage page)]
          [("title",hover), ("class","navButton")]
          [text name]

      about : PageType
      about =
        (About, "About", "Find out more about the Liberum Project.")

      favourites : PageType
      favourites =
        (Favourites, "Favourites", "Find your favourited threads here.")

      viewer : PageType
      viewer =
        (ThreadViewer, "Thread Viewer", "Read the last visited thread here")

      search : PageType
      search =
        (ThreadSearch, "Search", "Search for threads by name or tag.")

      contact : PageType
      contact =
        (Contact, "Contact", "Find our contact details here.")

      initial : PageType
      initial =
        (Initialize, "InitialPage", "Here For Testing Only, no back button.")

  export
  logo : Html
  logo =
    img
      [("title","Liberum - Speak Free and Unrestricted")
      ,("src","image/liberum-logo.svg")
      ,("width","275px")
      ,("height","110px")]
      []

  export
  sidebar : List Html -> Html
  sidebar content =
    div [("class", "sidebar")]
      [div [("class","logo")] [logo]
      ,div
        [("class", "sidebar-info")]
        (content ++
          [div
            [("class", "sidebar-icons")]
            [discordIcon,gitlabIcon]])]

  where
  
    discordIcon : Html
    discordIcon =
      linkImage
        "Liberum Central Discord Server Invitation"
        "https://discord.liberum.world"
        (MkImage "image/discord-logo.svg" (Pixels 100) (Pixels 35))

    gitlabIcon : Html
    gitlabIcon =
      linkImage
        "Liberum Codebase on Gitlab"
        "https://code.liberum.world"
        (MkImage "image/gitlab-logo.svg" (Pixels 35) (Pixels 35))

  export
  mainSection : List Html -> Html
  mainSection content =
    div [("class", "mainSection")] content

  export
  frame : List Html -> List Html -> Html
  frame side content =
    div
      [("class", "flex-box")]
      [sidebar side, mainSection content]

