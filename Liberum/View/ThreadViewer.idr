module Liberum.View.ThreadViewer
import Core.EventQueue
import Core.Utils
import Core.Types
import Core.Html

import Liberum.Types
import Liberum.View.Common
import Liberum.Model

parameters (evq : EventQueue Application, thread : Thread, prefs : UserPrefs)
  export
  msgSection : List Html
  msgSection =
      (navBar evq
        :: (map (encapsulate thread prefs)
           $ msgFilter (messages thread) prefs))

  export
  threadInfo : List Html
  threadInfo =
    [text "Thread Title"
    ,br [] []
    ,text $ title thread
    ,br [] []
    ,br [] []
    ,text "Original Poster"
    ,maybe (usertile $ default_user) (usertile) $ head' $ users thread
    ,br [] []
    ,text $ "Contributing Users: " ++ (show $ length $ users thread)
    ,br [] []
    ,br [] []
    ,text "Thread Tags"
    ,div [("class","sidebar-tag-container")] (threadTags thread prefs)]

  export
  threadPage : Html
  threadPage =
    frame evq threadInfo msgSection
