module Controller
import Core.Utils
import Core.EventQueue
import Core.Types
import Core.Primitives
import Core.Timer

import Liberum.Types
import Liberum.Model
import Liberum.View
import Liberum.View.About
import Liberum.View.ThreadViewer
import Liberum.View.Contact

%language ElabReflection

export
app : Node -> Html -> EventQueue Application -> JS_IO ()
app root last evq =
  do Just action <- pop evq
     | Nothing => (setTimeout (const $ app root last evq) 50 *> pure ())
     case action of

          (User (UserRequestPage Initialize)) =>
            do render root (Just last) (Just $ initial evq)
               app root (initial evq) evq

          (User (UserRequestPage About)) =>
            do render root (Just last) (Just $ aboutPage evq)
               app root (aboutPage evq) evq

          (User (UserRequestPage ThreadViewer)) =>
            do render root (Just last) (Just $ threadPage evq test_thread test_prefs)
               app root (threadPage evq test_thread test_prefs) evq

          (User (UserRequestPage Contact)) =>
            do render root (Just last) (Just $ contactPage evq)
               app root (contactPage evq) evq
          _ => trace "ignored action" $ app root last evq
